/*
В целом конструкцию try/catch можно использовать в коде везде где есть вероятность получить ошибку. Примерами могут быть получение каких то данных с сервера в которых мы не уверены (например неправильный JSON), или использвание каких то переменных, которые могут быть необъявлены или перетерты в другом месте.
*/

const books = [{
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.querySelector("#root");
const list = document.createElement("ul");
list.innerHTML = "Список валидных книг:"
list.style.fontSize = "24px";
root.appendChild(list);

function checkProp(array, obj, prop) {
    try {
        if (!obj.hasOwnProperty(prop)) {
            throw new Error;
        } 
    } catch (error) {
        console.log(`ERROR: element ${array.indexOf(obj) + 1} doesn't consist property "${prop}"`);
    }
}

books.forEach(book => {
    if ("author" in book && "name" in book && "price" in book) {
        const bookItem = document.createElement("li");
        bookItem.innerHTML = `${book.name}. Автор - ${book.author}`;
        bookItem.style.fontSize = "18px";
        bookItem.style.marginTop = "10px";
        list.appendChild(bookItem);
    } else {
        checkProp(books, book, "author"); 
        checkProp(books, book, "name"); 
        checkProp(books, book, "price"); 
    }
})